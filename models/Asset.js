const mongoose = require("mongoose");
const assetSchema = new mongoose.Schema({
			typeOfDevice : {
				type : String,
				required : [true, "Type of device is required"]
			},
			assetNumber : {
				type : String,
				required : [true, "Asset number is required"]
			},
			serialNumber : {
				type : String,
				default: null
			},
            hostName : {
				type : String,
				default: null
			},
            bitlockerPin : {
				type : String,
				default: null
			},
            bitlockerRecovery : {
				type : String,
				default: null
			},
            os : {
				type : String,
				default: null
			},
            ram : {
				type : Number,
				default: null
			},
            ssd : {
				type : Number,
				default: null
			},
            hdd : {
				type : Number,
				default: null
			},
            isActive : {
				type : Boolean,
				default: true
			},
            location : {
				type : String,
				default: null
			},
            note : {
				type : String,
				default: null
			},
		})
        
		module.exports = mongoose.model("Asset", assetSchema);