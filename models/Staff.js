const mongoose = require("mongoose");
const staffSchema = new mongoose.Schema({
			firstName : {
				type : String,
				required : [true, "Firstname is required"]
			},
			lastName : {
				type : String,
				required : [true, "Lastname is required"]
			},
			email : {
				type : String,
				default : null
			},
			position : {
				type : String,
				default : null
			},
			rank : {
				type : String,
				default : null
			},
			section : {
				type : String,
				default : null
			},
			division : {
				type : String,
				default : null
			},
			ipAddress : {
				type : String,
				default : null
			},
            assets: [
                {
                    assetId: {
                        type : String,
				        default : null
                    },
                    deployedAt: {
                        type : Date,
				        default : null
                    }
                }
            ]
		})
        
		module.exports = mongoose.model("Staff", staffSchema);