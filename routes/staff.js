const express = require("express");
const router = express.Router();
const staffController = require("../controllers/staff");
const auth = require("../auth");

// Route for staff creation
router.post("/register", auth.verify, (req, res) => {
	staffController.createStaff(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for getting users details
router.get("/", (req, res) => {
	staffController.getAllStaff().then(resultFromController => res.send(resultFromController));
});

// Route for updating staff details
router.get("/:id", auth.verify, (req, res) => {
	staffController.updateStaff(req.params).then(resultFromController => res.send(resultFromController));
});


module.exports = router;