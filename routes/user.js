const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for getting users details
router.get("/", (req, res) => {
	userController.getAllUserDetails().then(resultFromController => res.send(resultFromController));
});


module.exports = router;