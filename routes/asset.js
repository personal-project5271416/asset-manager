const express = require("express");
const router = express.Router();
const assetController = require("../controllers/asset");
const auth = require("../auth");

// Route for staff creation
router.post("/", auth.verify, (req, res) => {
	assetController.createAsset(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for getting assets details
router.get("/", (req, res) => {
	assetController.getAllAssets().then(resultFromController => res.send(resultFromController));
});

module.exports = router;