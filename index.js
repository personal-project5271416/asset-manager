const express = require("express");
const cors = require("cors");
const dotenv = require("dotenv").config();
const mongoose = require("mongoose");

const userRoutes = require("./routes/user");
const assetRoutes = require("./routes/asset");
const staffRoutes = require("./routes/staff");

const app = express();

mongoose.connect("mongodb+srv://henry-255:admin123@zuitt-bootcamp.wydh8oh.mongodb.net/asset-manager?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});


mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use("/users", userRoutes);
app.use("/assets", assetRoutes);
app.use("/staffs", staffRoutes);

// 404 NOT FOUND
app.all('*', (req,res) =>{
    res.send({message: "Something wen't wrong. The page you are looking for is not found"});
});

if(require.main === module){
    app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`);
    });
}
module.exports = app;