const Asset = require("../models/Asset");

// User Registration
module.exports.createAsset = async (reqBody) =>{
	try{
		return await Asset.findOne({assetNumber: reqBody.assetNumber}).then(result => {
			// User does not exist
			if(result == null){
	
				let newAsset = new Asset({
                    typeOfDevice: reqBody.typeOfDevice,
                    assetNumber: reqBody.assetNumber,
					serialNumber: reqBody.serialNumber,
					hostName: reqBody.hostName,
					bitlockerPin: reqBody.bitlockerPin,
					bitlockerRecovery: reqBody.bitlockerRecovery,
					os: reqBody.os,
					ram: reqBody.ram,
					ssd: reqBody.ssd,
					hdd: reqBody.hdd,
					location: reqBody.location
					
				})
				return newAsset.save().then((user, error) =>{
					if(error) {
						return  {message : error.message};
					} else {
						return  {message : "Asset created successfully"};
					};
				})
	
			// User exists
			} else {
				return  {message : "Asset already exists"};
			}
		})
	}catch(err){
		return {"message": err.message}
	}
}


// Get All Assets details
module.exports.getAllAssets = async () => {
	try{
		return await Asset.find({}).then(assets =>{
			return assets;
		})
	}catch(err){
		return {"message": err.message}
	}
	
}
