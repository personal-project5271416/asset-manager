const Staff = require("../models/Staff");

// User Registration
module.exports.createStaff = async (reqBody) =>{
	try{
		return await Staff.findOne({firstName : reqBody.firstName, lastName: reqBody.lastName}).then(result => {
			// User does not exist
			if(result == null){
	
				let newStaff = new Staff({
					firstName: reqBody.firstName,
					lastName: reqBody.lastName,
				})
				return newStaff.save().then((user, error) =>{
					if(error) {
						return  {message : error.message};
					} else {
						return  {message : "Staff created successfully"};
					};
				})
	
			// User exists
			} else {
				return  {message : "Account already registered"};
			}
		})
	}catch(err){
		return {"message": err.message}
	}
}

// Get All Staff details
module.exports.getAllStaff = async () => {
	try{
		return await Staff.find({}).then(staff =>{
			return staff;
		})
	}catch(err){
		return {"message": err.message}
	}
	
}

// Updating Staff details
module.exports.getAllStaff = async () => {
	try{
		return await Staff.find({}).then(staff =>{
			return staff;
		})
	}catch(err){
		return {"message": err.message}
	}
	
}
